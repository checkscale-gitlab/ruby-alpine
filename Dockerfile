FROM ruby:2.4-alpine

RUN apk --no-cache add python python-dev py-pip make gcc zip libgpg-error libassuan gpgme libxml2 libxslt-dev g++ groff less mailcap 
RUN pip install --upgrade pip 
RUN pip install --upgrade pyyaml awscli s3cmd python-magic
RUN apk -v --purge del py-pip
COPY Gemfile .
RUN bundle install
RUN gem install stack_master